if (confirm("Welcome! Do you want to start a new game?")) {
    var hero = {
        name: prompt("Enter your name"),
        class: undefined,
        damage: undefined,
        hitpoints: undefined,
        moral: undefined,
    }

    var enemy = {
        name:undefined,
        damage: undefined,
        hitpoints: undefined,
    }

    chooseClass();

    alert("Your class is " + hero.class + "\n Damage: " + hero.damage + "\n HP: " + hero.hitpoints + "\n Moral: " + hero.moral);

    while (confirm("Do you want to fight?")) {
        chooseEnemy();
        if (confirm("Your enemy is " + enemy.name + "\n Damage: " + enemy.damage + " HP: " + enemy.hitpoints + "\n Do you still want to fight?")) {
            if (fight()) {
                alert("You have won!"+ "\n Your HP is " + hero.hitpoints + "\n Your moral is: " + hero.moral);
            } else {
                alert("You have died");
                break;
            }
        } else {
            alert("👍️You have run away... \n -1 moral \n Your HP is " + hero.hitpoints + " \n Your moral is " + --hero.moral);
            if (hero.moral < 0) {
                break;
            }
        }

    }

    alert("Game over ☺️");
}

function chooseClass() {
    do {
        let userChoise = prompt("Which class do you want to choose:\n 1) Barbarian\n 2) Palladin");
        if (userChoise == "1") {
            hero.class = "Barbarian";
            break;
        } else if (userChoise == "2") {
            hero.class = "Palladin";
            break;
        }
    } while (true);

    if (hero.class == "Barbarian") {
        hero.damage = 10;
        hero.hitpoints = 50;
        hero.moral = 5;
    } else if (hero.class == "Palladin") {
        hero.damage = 6;
        hero.hitpoints = 60;
        hero.moral = 10;
    } else {
        prompt("Error 22");
    }
}

function chooseEnemy() {
    if (Math.random() < 0.7) {
        enemy.name = "Ork";
        enemy.damage = (Math.floor(Math.random()*(12-4)+4));
        enemy.hitpoints = (Math.floor(Math.random()*(80-20)+20));
    } else {
        enemy.name = "Troll";
        enemy.damage = 5;
        enemy.hitpoints = 20;
    };
}


function fight() {
    var roundCounter = 0;
    while (enemy.hitpoints > 0 && hero.hitpoints > 0) {
        if (!roundCounter%2) {
            hero.hitpoints -= enemy.damage;
        } else {
            enemy.hitpoints -= hero.damage;
        }
        roundCounter++;
    }
    if (hero.hitpoints > 0) {
        return true;
    } else {
        return false;
    }
}
