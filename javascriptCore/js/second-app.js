
if (confirm("Do you want to start \"Homework 12.19\" ?")) {
    do {
        fool_flag = false;
        let option = parseInt(prompt("Which task whould you like to check?\n 1) Запросите 2 числа и нарисуйте прямоугольник из X-ов, где первое число - это ширина, а второе - высота. \n\n 2) Запросите у пользователя n количество чисел (пользователь сам решает когда закончить ввод чисел), найдите и выведите самое большое число, самое маленькое, сумму всех чисел, среднее арифметическое, вывести отдельно все четные и нечетные числа (опционально). \n\n 3) Запросите у пользователя 2 числа и выведите все числа в промежутке между двумя числами (пользователь вводит 2 и 6, выводится 3,4,5). Вывести ошибку, если числа одинаковые. Учесть, что первое введенное число может быть больше первого. Запросить у пользователя каким способом вывести числа на экран ( в строку через пробел, через запятую или в столбик) \n\n 4) Запросите у пользователя число (число не должно быть меньше 10) и выведите все числа от 0 до заданного пользователем числа 3 разными циклами. каждый десятый элемент выводить красным цветом. Каждый третий - зеленым. Если элемент и по счету и третий и десятый - выводить синим цветом. \n\n 5) Нарисовать шахматную доску при помощи циклов (потребуются таблицы) \n\n 6) Нарисуйте в браузере с помощью буквы Х и циклов: прямоугольник, прямоугольный треугольник, равносторонний треугольник, ромб."))

        switch (option) {
            case 1:
                task_1();
                break;
            case 2:
                task_2();
                break;
            case 3:
                task_3();
                break;
            case 4:
                task_4();
                break;
            case 5:
                task_5();
                break;
            case 6:
                task_6();
                break;
            default:
                fool_flag = true;
        }
    } while (fool_flag);
}
//Так можно вообще делать или лучше создавать отдельную переменную homework_flag ,
//записать туда то, что передал confirm и работать уже с ней?
// https://bit.ly/35tHrN7



// Functions

function fool_protect (fool_index) {
    do {
        var fool_number = parseInt(prompt("Enter " + fool_index + " value:"));
        if (((typeof fool_number) != "number") || isNaN(fool_number)) {
            alert("Please enter a valid number.");
        } else {
            return fool_number;
        }
    } while (true);
}

// First task
function task_1() {
    var x = fool_protect("width");
    var y = fool_protect("height");

    document.write("<h2>First task</h2>");
    for (var i=0; i<y; i++) {
        for (var k=0; k<x; k++) {
            document.write("X");
        }
        document.write("<br>");
    }
}


// Second task
function task_2() {

    var temp = 0;
    var biggest = -Infinity;
    var smallest = Infinity;
    var sum = temp;
    var q = 0;
    var even = "Even numbers: ";
    var uneven = "Uneven numbers: ";

    do {
        temp = prompt("Enter a number:");

        if (temp === null) {
            break;
        }


        temp = parseInt(temp);
        if (isNaN(temp)) {
            if (confirm("Please enter a valid number. If you want to stop entering, then click \"Cancel\"")) {
                continue;
            }
            break;
        }

        if (temp > biggest) {
            biggest =temp;
        }

        if (temp < smallest) {
            smallest=temp;
        }

        if (temp%2) {
            uneven+=temp+" ";
        } else even+=temp+" ";

        sum+= temp;
        q++;
    } while (true)

    document.write("The biggest number is " + biggest);
    document.write("<br>");
    document.write("The smallest number is " + smallest);
    document.write("<br>");
    document.write("Sum is " + sum);
    document.write("<br>");
    document.write("Average is " + (sum/q));
    document.write("<br>");
    document.write(even);
    document.write("<br>");
    document.write(uneven);

}

// Third task
function task_3() {

    var a = fool_protect(1);
    var b = fool_protect(2);
    var result = "Numbers in between a and b are: <br> ";

    function display_option () {
        do {
            var display_property = parseInt(prompt("How do you want the numbers to be dispalyed:\n 1 – In line; \n 2 – Separated with comas: \n 3 – In column.\n Enter the needed number: "));
            switch (display_property){
              case (1):
                  return (" ");
              case (2):
                  return (", ");
              case (3):
                  return ("<br>");
              default:
                  alert("Please enter a valid option.");
                  display_property = NaN;
            }
        } while (isNaN(display_property));
    }

    var separator = display_option();

    document.write("<br>");


    if (a>b) {
        result += ++b;
        while (b < a-1){
            result+= separator + (++b);
        }
    }
    else if (b>a) {
        result += ++a;
        while (a < b-1){
            result += separator + (++a);
        }
    }
    else result = "Error: The numbers are equal!";

    document.write(result);

}

//Fourth task

function task_4() {

    do {
        var number = parseInt(prompt("Enter a number that is no less than 10."));
    } while (!(number>=10));

    var number_string = "0";

    // First cycle

    for (let i = 1; i <= number; i++) {
        let flag_3 = false;
        let flag_10 = false;
        let number_class;

        if (!((i+1)%3)) {
            flag_3 = true;
        }
        if (!((i+1)%10)) {
            flag_10 = true;
        }

        if (flag_3 && flag_10) {
            number_class = "blue";
        } else if (flag_3) {
            number_class = "green";
        } else if (flag_10) {
            number_class = "red";
        } else {
            number_class = "normal";
        }

        number_string += "<div class = " + number_class + ">" + i + "</div>";
    }

    document.write(number_string);
    document.write("<br>");


    // Second cycle
    number_string = "0";
    var number_counter = 0;

    do {
        number_counter++;

        let flag_3 = false;
        let flag_10 = false;
        let number_class;

        if (!((number_counter+1) %3 )) {
            flag_3 = true;
        }
        if (!((number_counter+1) % 10)) {
            flag_10 = true;
        }

        if (flag_3 && flag_10) {
            number_class = "blue";
        } else if (flag_3) {
            number_class = "green";
        } else if (flag_10) {
            number_class = "red";
        } else {
            number_class = "normal";
        }

        number_string += "<div class = " + number_class + ">" + number_counter + "</div>";


    } while (number_counter < number);

    document.write(number_string);
    document.write("<br>");

    // Third cycle

    number_string = " ";
    while (number > 0) {
        let flag_3 = false;
        let flag_10 = false;
        let number_class;

        if (!((number+1) %3 )) {
            flag_3 = true;
        }
        if (!((number+1) % 10)) {
            flag_10 = true;
        }

        if (flag_3 && flag_10) {
            number_class = "blue";
        } else if (flag_3) {
            number_class = "green";
        } else if (flag_10) {
            number_class = "red";
        } else {
            number_class = "normal";
        }

        number_string = "<div class = " + number_class + ">" + number + "</div>" + number_string;
        number--;
    }

    document.write("0" + number_string);
}

//Task 5

function task_5() {
    document.write ("<table cellspacing=\"0\" cellpadding=\"0\" class = chessboard>" + chessboard(8) + "</table>");

    function chessboard(cell) {
        var cellcolor;
        var table_body = "";

        for (let i = 0; i < cell - 1; i++) {
            table_body += ("<tr>" + chesscolumn(i) + "</tr>");
        }

        function chesscolumn (chessrow){
            var table_row = "";
            for (let k = 0; k < cell-1; k++) {
                if ((chessrow + k) % 2) {
                    cellcolor = "black";
                } else {
                    cellcolor = "white";
                }
                table_row += ("<td class = " + cellcolor + "></td>");
            }
            return table_row;
        }

        return table_body;
    }
}

//Task 6

function task_6() {
    // прямогольник

    for (let i = 0; i < 4; i++) {
        for (let k = 0; k < 10; k++) {
            document.write("X");
        }
        document.write("<br>");
    }

    document.write("<br>");
    // "прямоугольный" треугольник


    const LETTER_INVISIBLE = "<span class = invisible>X</span>";

    for (let i = 0; i < 4; i++) {
        for (let k = 0; k < 4 - i; k ++) {
            document.write(LETTER_INVISIBLE);
        }
        for (let k = 0; k < 1 + 2 * i; k++) {
            document.write("X");
        }
        document.write("<br>");
    }

    // "равносторонний" треугольник

    for (let i = 0; i < 5; i++) {
        for (let k = 0; k < i; k ++) {
            document.write("X");
        }
        document.write("<br>");
    }

    document.write("<br>");


    // ромб

    for (let i = 0; i < 4; i++) {
        for (let k = 0; k < 4 - i; k ++) {
            document.write(LETTER_INVISIBLE);
        }
        for (let k = 0; k < 1 + 2 * i; k++) {
            document.write("X");
        }
        document.write("<br>");
    }
    for (let i = 2; i >= 0; i--) {
        for (let k = 0; k < 4 - i; k ++) {
            document.write(LETTER_INVISIBLE);
        }
        for (let k = 0; k < 1 + 2 * i; k++) {
            document.write("X");
        }
        document.write("<br>");
    }
}
