let xhr = new XMLHttpRequest;

xhr.open("GET","https://jsonplaceholder.typicode.com/comments?postId=1");
xhr.send();

xhr.onload = function() {
  const nodeJSON = document.createElement("div");
  nodeJSON.innerText = xhr.response;
  document.body.append(nodeJSON);
}
