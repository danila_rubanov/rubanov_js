window.onload = function () {
  const textRegexp = /\?text=([A-Za-z0-9_z%+.-]*)&?/u;
  const url = location.search;
  const encodedText = url.match(textRegexp);

  const decodedText = decodeURIComponent(encodedText[1]);
  const textNode = document.getElementById("text");
  textNode.textContent = decodedText;
}
