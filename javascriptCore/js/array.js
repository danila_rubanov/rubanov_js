
task1();

task_2();

task_3();

task_4();

function task1() {
    var arrayA = [];
    var arrayB = [];

    for (let i = 0; arrayA[i-1] !== null ; i++) {
        arrayA[i] = prompt("Enter a value");
    }

    arrayA.pop();

    if (confirm("Do you want to make a copy of the array?")) {
        array_copy(arrayA,arrayB);
    }

    function array_copy(array_q,array_b) {
        for (let i = 0; i < array_q.length; i++) {
            array_b[i] = array_q[i];
        }
    }

    document.write(arrayB);
}

function task_2() {
    var array_t2 = [];

    for (let i = 0; array_t2[i-1] !== null ; i++) {
        array_t2[i] = fool_protect("Enter a number");
    }

    array_t2.pop();
    array_sort(array_t2);
    document.write(array_t2.join(" "));
    //


    function array_sort(sortedArray) {
        var sort_flag;
        do {
            sort_flag = false
            for (var i = 0; i < sortedArray.length; i++) {
                if (sortedArray[i] > sortedArray[i+1]) {
                    let temp = sortedArray[i];
                    sortedArray[i] = sortedArray[i+1];
                    sortedArray[i+1] = temp;
                    sort_flag = true;
                }
            }
            } while (sort_flag);
    }
}

function task_3() {
    //Creating an array

    var array_t3 = new Array(2);
    for (let i = 0; i < array_t3.length; i++) {
        array_t3[i] = new Array();
    }

    // Filling the array with data
    t3_input();

    // Getting indexes of emtpy cells
    var empty_cells = t3_empty_check(array_t3);

    //Filling empty cells
    if (confirm("Do you want to fill empty cells?")) {
        t3_fill_empty(array_t3,empty_cells);
    }

    // Output
    for (let i = 0; i < array_t3[0].length; i++) {
        if (array_t3[1][i] == "" || array_t3[1][i] == null) {
            array_t3[1][i] = "нет информации";
        }
        document.write(array_t3[0][i] + ": " + array_t3[1][i] + "<br>");
    }


    function t3_input() {
        var i = 0;

        do {
            var cont_flag = true;

            do {
                array_t3[0][i] = prompt("Enter a Name");
            } while (array_t3[0][i] === null || array_t3[0][i] === "");

            array_t3[1][i] = prompt("Enter a Phone number");

            cont_flag = confirm("Continue entering?");
            i++;
        } while (cont_flag);
    }

    function t3_empty_check(array_q) {
        var array_e = [];
        for (let i = 0; i < array_q[0].length; i++) {
            if (array_q[1][i] == "" || array_q[1][i] == null) {
                array_e.push(i);
            }
        }
        return array_e;
    }

    function t3_fill_empty(array_q,array_e) {
        for (let i = 0; i < array_e.length; i++) {
            array_q[1][array_e[i]] = prompt("Enter a phone number for " + array_q[0][array_e[i]]);
        }
    }

}

function task_4() {
    var array_t4 = new Array;
    var i_t4 = 0;

    //Inputting an array

    do {
        array_t4[i_t4] = fool_protect("Enter a number");
        i_t4++;
    } while (array_t4[i_t4-1] !== null);

    array_t4.pop();

    //Getting duplicated numbers and their indexes
    var recurrring_numbers = duplicated_numbers(array_t4);

    for (let i = 0; i < recurrring_numbers[0].length; i++) {
        document.write("<br> Recurring number: <b>" + recurrring_numbers[0][i] + "</b> Its indexes: " + "<em>" + recurrring_numbers[1][i].join("</em> , <em>") + "</em>");
    }

    function duplicated_numbers(array_q) { //возвращает массив array_d в котором хранятся числа в array_d[0] и индексы этих чисел, которые хранятся в array_d[1][i]
        var array_d = new Array;
        for (let i = 0; i < 2; i ++) {
            array_d[i] = new Array;
        }

        for (let i = 0; i < array_q.length-1; i++) {
            var k = array_d[0].length;
            var temp = array_q[i];
            var duplicate_flag = false;
            if (temp === null) continue;
            array_d[1][k] = new Array;


            for (let j = i + 1; j < array_q.length; j++) {
                if (temp == array_q[j]) {
                    array_d[0][k] = array_q[i];
                    array_d[1][k].push(j);
                    console.log(array_d[1][k]);
                    array_q[j] = null;
                    duplicate_flag = true;
                }
            }

            if (duplicate_flag) {
                array_d[1][k].unshift(i);
            }
        }

        return array_d;
    }
}

function fool_protect(fool_string) {
    do {
        var fool_number = prompt(fool_string);

        if (fool_number === null) {
            return null;
        }

        fool_number = parseInt(fool_number);

        if (isNaN(fool_number)) {
            alert("Please enter a number!");
        } else {
            return fool_number;
        }
    } while (true);
    return fool_number;
}
