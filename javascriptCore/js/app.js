var homework_flag = confirm("Do you want to start \"Homework 12.19\" ?");

if (homework_flag) {
  homework_1219();
}

function homework_1219 () {
  //FIRST Task

  var a = parseInt(prompt("1 number is: "));
  var b = parseInt(prompt("2 number is: "));

  var A = "<b> 1 number </b>";
  var B = "<b> 2 number </b>";

  document.write("<h2>First Task</h2>");
  document.write("<pre>");
  document.writeln("Sum of "+A+" and "+B+" is "+(a+b)+".");
  document.writeln("Difference of "+A+" and "+B+" is "+(a-b)+".");
  document.writeln("Product of "+A+" and "+B+" is "+(a*b)+".");
  document.writeln("Quotient of "+A+" and "+B+" is "+(a/b)+".");
  document.write("</pre>");

  // SECOND Task

  var sum = a + b;

  for (var x=1; x<4; x++){
      sum += parseInt(prompt((x+2)+" number is: "));
  }

  document.write("<h2>Second Task</h2>");
  document.write("<pre>");
  document.write("Average of 5 numbers is "+(sum/5)+".");
  document.write("</pre>");

  // THIRD Task

  var h = parseInt(prompt("Height is: "));
  var r = parseInt(prompt("Radius is: "));
  document.write("<h2>Third Task</h2>");
  document.write("<pre>");
  document.writeln("Volume is "+(Math.PI*r*r*h)+".");
  document.writeln("Surface area is "+(2*Math.PI*r*r+2*Math.PI*r*h)+".");
  document.write("</pre>");

  // FOURTH Task

  var sum_ev = 0;

  for (var x=0; x<5; x++){
      sum_ev += parseInt(prompt((x+1)+" number is: "));
  }

  document.write("<h2>Fourth Task</h2>");

  if (sum_ev%2) {
      document.write("Sum of 5 numbers is uneven.")
  }
  else if (isNaN(sum_ev)) {
      document.write("Error.");
  }
  else
  document.write("Sum of 5 numbers is even.");

  // FIFTH Task

  prompt("Please enter your\nName:");
  var sur = prompt("Please enter your\nSurname:");
  if (prompt("Please enter your\nAge:")<18) {
      alert("Sorry, "+sur+ " you are not allowed to access this page.");
  };

  // SIXTH Task
  var weekday = prompt("What day is it today?");

  document.write("<h2>Sixth Task</h2>");
  document.write("<i>Today is </i>");
  switch (weekday) {
      case '1':
          document.write('Monday');
          break;
      case '2':
          document.write('Tuesday');
          break;
      case '3':
          document.write('Wednesday');
          break;
      case '4':
          document.write('Thursday');
          break;
      case '5':
          document.write('Friday');
          break;
      case '6':
          document.write('Saturday');
          break;
      case '7':
          document.write('Sunday');
          break;
      default:
          document.write('Error.');
  }
}
