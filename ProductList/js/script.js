const productTemplate = (product) => 
            `<div class="product_item">
                <img class="product_image" src=${product.img}>
                <h3>${product.name}</h3>
                <span>${product.price}</span>
            </div>`;

window.onload = () => {
    const dom = {
        minicart: document.querySelector("#minicart"),
        productList: document.querySelector("#product_list")
    }

    let xhr  = new XMLHttpRequest();

    xhr.open("GET", "https://zombee.space/hillel/products.json");
    xhr.send();

    xhr.onload = () => {
        const products = JSON.parse(xhr.response);
        let productsList = "";

        for (let product of products) {
            console.log(product);
            productsList += productTemplate(product);
        }
        dom.productList.innerHTML = productsList;
    }
}